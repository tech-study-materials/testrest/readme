# Declarative REST clients in tests
## **_Strongly typed REST calls, a.k.a. Who needs RestAssured._**

* Are your integration tests maintainable? Fragile? 
* Are they refactoring-friendly?
* When do you find out that you broke them (although production code is working fine)? 
    - At compile time? 
    - Or runtime? 

Let's see how to improve.

---

# Let's start...
## ...by writing a SpringMVC app and few tests
[Here it is](https://gitlab.com/tech-study-materials/testrest/spring/-/blob/master/src/main/java/com/example/todoer/TodoerApplication.java)

Issues:

* can you refactor models/dtos without breaking your tests?
* can you refactor webmethod signatures without breaking your tests?
* can you refactor urls without breaking your tests?
* **and most importantly - when your tests do break, is it evident how to fix them?**

---

# Generate it!
## _I wish someone wrote the http-handling code for me, so I can just use my java interfaces..._

You're in luck! You just need to expose a contract and run a code generator, [like here](https://gitlab.com/tech-study-materials/testrest/generated/-/tree/master/)

Now, try to refactor something, e.g. remove {user} from 'DELETE todo'.

Clunky process, isn't it?

---

# Declare it!
## _There has to be a better way! What do other application frameworks offer?_

Yeah, let's see. [Micronaut is cool!](https://gitlab.com/tech-study-materials/testrest/micronaut/-/tree/master/)

---

# Declare it!
## _Take me back to Spring world, it must have something like that too!_

Of course it does, [have a look](https://gitlab.com/tech-study-materials/testrest/spring/-/tree/feign)

---

# What if...
## _...I'm in JEE world?_

Any spec or framework that uses annotated interfaces to declare http endpoints can benefit from declarative approach.

Here's an [example with JAX-RS stack](https://gitlab.com/tech-study-materials/testrest/jee/-/tree/master).

---

# What if...
## _...my code doesn't use interface-based endpoint declarations?_

You mean sparkjava or java-express?
 
Or are you serving REST via Servlet API? (in that case, I'm sorry for you) 

[You still can use declarative rest clients](https://gitlab.com/tech-study-materials/testrest/express/-/blob/master/src/test/java/com/example/todoer/FeignTests.java), but compiler will no longer help you to the same extent.
Test code will still be easier to maintain and work with, although not immune to API changes.

---

# What if...
## _...I want a declarative client BUT ALSO access to low-level http information (e.g. status codes)?_

In that case you might find [this](https://gitlab.com/tech-study-materials/testrest/express/-/blob/master/src/test/java/com/example/todoer/RetrofitTests.java) interesting.

Not as safe as shared java interface (shared between server and rest client), but still useful.

---

# Bonus

Use the same test code as fast (zero-I/O) 'unit' tests, as well as more thorough integration tests.

* [Micronaut example](https://gitlab.com/tech-study-materials/testrest/micronaut/-/tree/bonus)
* [Spring example](https://gitlab.com/tech-study-materials/testrest/spring/-/tree/bonus)
* [JAX-RS example](https://gitlab.com/tech-study-materials/testrest/jee/-/tree/bonus)

Just run the tests with or without:
 
 `-Dintegration-testing=true`

---

# Resources

* [OpenApi generator](https://openapi-generator.tech/)
* [OpenFeign](https://github.com/OpenFeign/feign)
* [OpenFeign in Spring](https://cloud.spring.io/spring-cloud-openfeign/reference/html/)
* [Compatibility table of Cloud&Boot (Table 1)](https://spring.io/projects/spring-cloud)
* [MicroProfile Rest Client](https://github.com/eclipse/microprofile-rest-client)
* [Retrofit](https://square.github.io/retrofit/)

---

# Thank you!

I'm @ [https://gitlab.com/tech-study-materials/testrest/readme](https://gitlab.com/tech-study-materials/testrest/readme)